package com.google.ar.sceneform.samples.solarsystem.raumvektorsystem;

import java.util.ArrayList;

public class RaumSystem
{
    private ArrayList<Raum> raeume;

    public RaumSystem()
    {
        this.raeume = new ArrayList<>(0);
    }

    public void addRaum(Raum r)
    {
        if(!raeume.contains(r))
        {
            raeume.add(r);
        }
    }

    public Raum sucheRaum(String name)
    {
        return raeume.stream().filter(r -> r.getId().equals(name)).findAny().orElse(null);
    }

    public void rekalkuliereRaumkoordinaten(Vektor nullpunkt)
    {
        for (Raum r : raeume)
        {
            r.getKoordinaten().subtrahiere(nullpunkt);
        }
    }

    public void printRaeume()
    {
        for(Raum r : raeume)
        {
            System.out.println(r.toString());
        }
    }

    public ArrayList<Raum> getRaeume()
    {
        return raeume;
    }
}

