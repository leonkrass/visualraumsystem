package com.google.ar.sceneform.samples.solarsystem.raumvektorsystem;

public class Vektor
{
    private double x, y, z;

    public Vektor(double x, double y, double z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj instanceof Vektor)
        {
            Vektor v = (Vektor) obj;
            return v.getX() == x &&
                    v.getY() == y &&
                    v.getZ() == z;
        }

        return false;
    }

    public void addiere(Vektor zuAddieren)
    {
        this.x += zuAddieren.getX();
        this.y += zuAddieren.getY();
        this.z += zuAddieren.getZ();
    }

    public void subtrahiere(Vektor zuSubtrahieren)
    {
        Vektor v = new Vektor(-zuSubtrahieren.getX(), -zuSubtrahieren.getY(), -zuSubtrahieren.getZ());
        this.addiere(v);
    }

    public double getX()
    {
        return x;
    }

    public double getY()
    {
        return y;
    }

    public double getZ()
    {
        return z;
    }

    public void setX(double x)
    {
        this.x = x;
    }

    public void setY(double y)
    {
        this.y = y;
    }

    public void setZ(double z)
    {
        this.z = z;
    }
}
