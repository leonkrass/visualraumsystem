package com.google.ar.sceneform.samples.solarsystem.raumvektorsystem;

public enum RaumTyp
{
    KLASSENRAUM,
    MEDIENRAUM,
    LABOR,
    WC_HERREN,
    WC_DAMEN
}
