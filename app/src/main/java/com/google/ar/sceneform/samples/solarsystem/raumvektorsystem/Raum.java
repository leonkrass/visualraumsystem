package com.google.ar.sceneform.samples.solarsystem.raumvektorsystem;

public class Raum
{
    private String id;
    private RaumTyp typ;
    private Vektor koordinaten;

    public Raum(String id, RaumTyp typ, Vektor koordinaten)
    {
        this.id = id;
        this.typ = typ;
        this.koordinaten = koordinaten;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj instanceof Raum)
        {
            Raum r = (Raum) obj;
            return r.getId().equals(this.id);
        }

        return false;
    }

    @Override
    public String toString()
    {
        return String.format("%-10s (%12s):\t {%2.4f | %2.4f | %2.4f}",
                this.id,
                this.typ.toString(),
                this.koordinaten.getX(),
                this.koordinaten.getY(),
                this.koordinaten.getZ());
    }

    public String getId()
    {
        return id;
    }

    public RaumTyp getTyp()
    {
        return typ;
    }

    public Vektor getKoordinaten()
    {
        return koordinaten;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public void setTyp(RaumTyp typ)
    {
        this.typ = typ;
    }

    public void setKoordinaten(Vektor koordinaten)
    {
        this.koordinaten = koordinaten;
    }
}
