package com.google.ar.sceneform.samples.solarsystem.raumvektorsystem;

public class Demo
{
    public static void main(String[] args)
    {
        Raum WcHerren = new Raum("WC Herren", RaumTyp.WC_HERREN, new Vektor(-9.5d, 3.0d, 0.0d));
        Raum WcDamen = new Raum("WC Damen", RaumTyp.WC_DAMEN, new Vektor(-7.0d, 3.0d, 0.0d));
        Raum R164 = new Raum("R164", RaumTyp.KLASSENRAUM, new Vektor(-5.0d, 3.0d, 0.0d));
        Raum R165 = new Raum("R165", RaumTyp.KLASSENRAUM, new Vektor(3.0d, 3.0d, 0.0d));
        Raum R167 = new Raum("R167", RaumTyp.KLASSENRAUM, new Vektor(16.0d, 3.0d, 0.0d));
        Raum R144 = new Raum("R144", RaumTyp.LABOR, new Vektor(-9.0d, 0.0d, 0.0d));
        Raum R166 = new Raum("R166", RaumTyp.MEDIENRAUM, new Vektor(0.0d, 0.0d, 0.0d));
        Raum R166a = new Raum("R166a", RaumTyp.KLASSENRAUM, new Vektor(16.0d, 0.0d, 0.0d));

        RaumSystem rs = new RaumSystem();
        rs.addRaum(WcHerren);
        rs.addRaum(WcDamen);
        rs.addRaum(R164);
        rs.addRaum(R165);
        rs.addRaum(R167);
        rs.addRaum(R144);
        rs.addRaum(R166);
        rs.addRaum(R166a);

        rs.printRaeume();
        System.out.println();

        rs.rekalkuliereRaumkoordinaten(R166a.getKoordinaten());
        rs.printRaeume();
    }
}

